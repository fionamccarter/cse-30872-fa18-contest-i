#!/usr/bin/env python3

import sys

indexes = {}

def largest_perm(nums, k, n):

    k = min(k,n)

    #record where each number is
    for i, num in enumerate(nums):
        indexes[num] = i    
    
    # swap largest with beginning
    largest = str(n)
    swaps =0
    i=0

    while swaps < k and i < len(nums):
        if nums[i] != largest:
            #swap
            swaps +=1
            ind = indexes[largest]
            temp = nums[i]
            nums[i] = nums[ind]
            nums[ind] = temp
            indexes[temp] = ind
            indexes[largest] = i

        
        #go down to next num
        largest = str(int(largest)-1)
        i +=1

    return ' '.join(map(str, nums))


if __name__ == '__main__':

    lines =list(sys.stdin)

    while (len(lines)):
        n,k = list(map(int, lines.pop(0).split()))
        perm = list(lines.pop(0).split())
        print(largest_perm(perm, k, n))
    


