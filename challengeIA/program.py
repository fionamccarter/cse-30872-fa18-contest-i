#!/usr/bin/env python3
import sys

def viewable(heights):

    if len(heights) == 0:
        return 0

    highest = heights[len(heights)-1]
    ct = 1
    for i in range(len(heights)-1)[::-1]:
        if heights[i] > highest:
            ct +=1
            highest = heights[i]

    return ct;


if __name__ == "__main__":
    for line in sys.stdin:
        heights = list(map(int, line.strip().split()))

        print(viewable(heights))

