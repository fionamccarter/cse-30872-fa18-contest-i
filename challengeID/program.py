#!/usr/bin/env python3

if __name__ == '__main__':
    line=input()

    while(line!=''):
        #parse input
        line=line.strip().split(' ')
        numpots=int(line[0])
        flowers=int(line[1])
        pots=[]
        
        #append pots
        for pot in range(int(numpots)):
            pots.append(int(input().strip()))
    
        pots.sort()
        right=pots[-1]
        best = (right-1) // (flowers-1) #best possible situation
        index =1

        #see if you can plant all flowers 'best' pots apart. if not, shrink best by 1 til it works
        currflowers = flowers
        while best > 1:
            while currflowers and index <= right:
                if index in pots:
                    currflowers -=1
                    index += best
                else:
                    index+=1

            if currflowers:
                best -=1
                index =1
                currflowers = flowers
            else:
                break

        #print answer
        print(best-1)

        #try to collect more input
        try:
            line=input()
        except EOFError:
            break

